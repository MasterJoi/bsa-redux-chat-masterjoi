import * as React from "react";
import { Navbar } from 'react-bootstrap';
import Container from "react-bootstrap/Container";
import 'bootstrap/dist/css/bootstrap.min.css';


class Header extends React.Component {

    getFormatDate(message) {
        return message.slice(0, 10) + " " + message.slice(11, 16);
    }

    render() {
        return (
            <Container className='header'>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" style={{padding: '10px'}}>
                    <Navbar.Brand href="#home">MasterJOI`s Chat</Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                            Signed in as: <a href="#login">Kyryll Hlum</a>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Navbar>
                <div className="d-flex align-items-center justify-content-around">
                    <h3 className="header-title text-center py-3 d-inline">
                        Task Chat
                    </h3>
                    <span className='header-users-count'>{this.props.participantsNumber} participants</span>
                    <span className='header-messages-count'>{this.props.messagesNumber} messages</span>
                    <span className='header-last-message-date'>last message at: {this.getFormatDate(this.props.lastMessage)}</span>
                </div>
            </Container>
        )
    }

}
export default Header;