import React from 'react';
import Chat from './chat/Chat'
import EditMessageModal from './editMessageModal/EditMessageModal';

function App() {
  return (
    <div className="App">
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
      <EditMessageModal />
      </div>
  );
}

export default App;
