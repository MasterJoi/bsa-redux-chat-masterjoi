import * as React from "react";
import Container from "react-bootstrap/Container";
import Message from "../messages/Message"
import OwnMessage from "../messages/OwnMessage";
import moment from 'moment';
import { Card } from "react-bootstrap";

class MessageList extends React.Component {
    render() {
        let last = false;
        return (
            <Container>
                <div className="message-list">
                    {
                        <ul className="list-group" style={{
                            marginBottom: "60px", width: "100%",
                            marginTop: "10px"
                        }}>
                            {this.props.numberOfMessages > 0 ? (
                                this.props.messages.map((item, i) => {
                                    const isCurrentUser = this.props.currentUser.userId === item.userId;
                                    if (i === this.props.numberOfMessages - 1) last = true;
                                    let dateForUser = item.createdAt.slice(11, 16);

                                    let previousDate = "";
                                    if (i > 0) {
                                        previousDate = moment(this.props.messages[i - 1].createdAt.slice(0, 10)).format(
                                            "L"
                                        );
                                    } else {
                                        previousDate = moment('2020-07-15').format("L");
                                    }

                                    let currentDate = moment(item.createdAt.slice(0, 10)).format("L");

                                    if (!isCurrentUser) {
                                        return (
                                            <Container className="d-flex " style={{ flexDirection: 'column' }}>
                                                {previousDate && !moment(currentDate).isSame(previousDate, "day") ? (
                                                    <Card className="w-100 d-flex justify-content-between align-items-center">
                                                        {moment(currentDate).format('LL')}
                                                    </Card>) : null}
                                                <div style={{ alignSelf: "flex-start" }}>
                                                    <Message item={item} onLike={this.props.likeMessageHandler} date={dateForUser} index={i} />
                                                </div>
                                            </Container>
                                        )

                                    } else {
                                        return (
                                            <Container className="d-flex " style={{ flexDirection: 'column' }}>

                                                {previousDate && !moment(currentDate).isSame(previousDate, "day") ? (
                                                    <Card className="w-100 d-flex justify-content-between align-items-center">
                                                        {moment(currentDate).format('LL')}
                                                    </Card>) : null}
                                                <div style={{ alignSelf: "flex-end" }}>
                                                    <OwnMessage item={item} isLast={last} onEdit={this.props.editMessageHandler} onDelete={this.props.deleteMessageHandler} date={dateForUser} index={i} />
                                                </div>
                                            </Container>
                                        )
                                    }
                                })) : (
                                <div className="text-center mt-5 pt-5">
                                    <p className="lead text-center">Fetching Messages</p>
                                </div>
                            )}
                        </ul>
                    }
                </div>
            </Container>
        )
    }
}

export default MessageList;