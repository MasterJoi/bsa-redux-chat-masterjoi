import * as React from "react";
import { Spinner } from "react-bootstrap";

const Preloader = () => {
    return <div className="preloader" >
        <Spinner animation="grow" variant="dark" />
    </div>
}


export default Preloader;