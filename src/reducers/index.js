import { combineReducers } from "redux";
import chat from "../chat/reducer"
import editMessageModal from "../editMessageModal/reducer"


const rootReducer = combineReducers({
    chat,
    editMessageModal
});

export default rootReducer;