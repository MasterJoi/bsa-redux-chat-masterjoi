import * as React from "react";
import { connect } from 'react-redux';
import * as actions from './actions';
import { editMessage } from '../chat/actions'
import { Container, Modal } from "react-bootstrap";
import { Form } from 'react-bootstrap';
import Button from "react-bootstrap/Button";


class EditMessageModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        };
        this.handleCancelClick = this.handleCancelClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.messageId !== this.props.messageId && nextProps.messageId !== '') {
            const mess = this.props.messages.find(el => el.id === nextProps.messageId);
            this.setState({
                text: mess.text
            });
        }
    }

    handleCancelClick() {
        this.props.dropCurrentMessageId();
        this.props.hidePage();
    }

    handleInputChange(e) {
        this.setState({
            text: e.target.value
        })
    }

    handleSaveClick() {
        this.props.editMessage({ id: this.props.messageId, text: this.state.text });
        this.props.dropCurrentMessageId();
        this.props.hidePage();
        this.setState({
            text: ''
        });
    }

    getEditPageContent() {
        return (
            <Container className="edit-message-modal">
                <Modal show={true} className="modal-shown">
                    <Modal.Header>
                        <Modal.Title>Edit Your message!</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Control className="edit-message-input"
                            value={this.state.text}
                            style={{ width: "100%" }}
                            required
                            type="text"
                            placeholder="Type Message here..."
                            onChange={this.handleInputChange}
                        />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleCancelClick} className="edit-message-button">
                            Cancel
                        </Button>
                        <Button variant="primary" onClick={this.handleSaveClick} className="edit-message-close">
                            Save Changes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </Container>
        );
    }

    render() {

        const isShown = this.props.isShown;
        console.log(isShown);
        return isShown ? this.getEditPageContent() : null;
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat,
        isShown: state.editMessageModal.isShown,
        messageId: state.editMessageModal.messageId,
        text: state.editMessageModal.text
    }
};

const mapDispatchToProps = {
    ...actions,
    editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);