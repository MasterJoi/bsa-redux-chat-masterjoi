import * as React from "react";
import Header from "../header/Header";
import MessageList from "../messageList/MessageList";
import MessageInput from "../messageInput/MessageInput";
import Preloader from "../preloader/Preloader";
import { connect } from 'react-redux';
import * as actions from './actions';
import { showPage, setCurrentMessageId } from '../editMessageModal/actions'

 const SpinnerDivStyle = {
    display: 'flex',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
}

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.handleSendClick = this.handleSendClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.handleLikeMessage = this.handleLikeMessage.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
    }

    async componentDidMount() {
        let messages = await fetch(this.props.url).then(resp => resp.json());
        this.props.getMessages(messages);
        window.addEventListener('keydown', (ev) => {
            if (ev.key === 'ArrowUp' && this.props.messages[this.props.messages.length - 1].user === this.getCurrentUser().user) {
                this.handleEditClick(this.props.messages[this.props.messages.length - 1].id);
            }
        })
    }

    handleLikeMessage(id) {
        this.props.likeMessage(id);
    }

    handleDeleteClick(id) {
        this.props.deleteMessage(id);
    }

    handleEditClick(id) {
        let currentMessageText = this.props.messages.find(el => el.id === id).text;
        this.props.setCurrentMessageId(id, currentMessageText);
        this.props.showPage();
    }

    handleSendClick(newMessage) {
        this.props.sendMessage(newMessage);
    }

    getParticipantsNumber(messages) {
        let userNames = messages.map((elem) => { return elem.user; });
        let uniqueUserNames = userNames.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });

        return uniqueUserNames.length;
    }

    getCurrentUser() {
        const currentUser = {
            user: "Kirill",
            userId: "121314",
            avatar: null
        }
        return currentUser;
    }

    render() {
        
        return (
            (JSON.stringify(this.props.messages) === JSON.stringify([])) ?
                <div style={SpinnerDivStyle}><Preloader/></div > :
            <div className='chat'>
                <Header
                    participantsNumber={this.getParticipantsNumber(this.props.messages)}
                    messagesNumber={this.props.messages.length}
                    lastMessage={this.props.messages[this.props.messages.length - 1].createdAt}
                />
                <MessageList
                    numberOfMessages={this.props.messages.length}
                    messages={this.props.messages}
                    currentUser={this.getCurrentUser()}
                    likeMessageHandler={this.handleLikeMessage}
                    deleteMessageHandler={this.handleDeleteClick}
                    editMessageHandler={this.handleEditClick}
                />
                <MessageInput
                    sendHandler={this.handleSendClick}
                    currentUser={this.getCurrentUser()}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat
    }
};
const mapDispatchToProps = {
    ...actions,
    showPage,
    setCurrentMessageId
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);