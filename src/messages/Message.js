import * as React from "react";
import Button from "react-bootstrap/Button";
import Avatar from 'react-avatar';
import { HeartFill } from 'react-bootstrap-icons';

const CardFooter = {
    width: "100%",
    display: "flex",
    alignItems: "center",
}

const CardFotterChildStyle = {
    flex: "1",
}

class Message extends React.Component {
    render() {
        return (
            <li className="message list-group-item" key={this.props.item.userId} style={{ alignSelf: "flex-start", margin: "10px", border: "1px solid #1d1f1c", borderRadius: "30px" }}>
                <Avatar className='message-user-avatar' style={{ marginRight: "10px" }} size="50" src={this.props.item.avatar} round={true} />
                <strong className='message-user-name'>{this.props.item.user}</strong>
                <p className='message-text'>{this.props.item.text}</p>
                <div style={CardFooter}>
                    <div style={CardFotterChildStyle} >
                        {
                            <Button className='message-like' variant="dark" onClick={() => { this.props.onLike(this.props.item.id) }} style={{ margin: "5px" }}>
                                <HeartFill color={this.props.item.isLiked ? "red" : "white"} />
                            </Button>
                        }
                    </div>
                    <footer style={{ alignSelf: 'flex-end' }} className="blockquote-footer">
                        <cite className='message-time'>{this.props.date}</cite>
                    </footer>
                </div>
            </li>
        )
    }
}

export default Message;